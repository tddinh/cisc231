
package api.client;

import java.util.UUID;


public class Client {
  private UUID uuid;
  private String name;
  private String address;
  private String state;
  private String country;
  private String date_of_birth;
  private int phone_number;

  public Client(String name, String address, String country, String state, String dob, int phone) {
    // https://www.baeldung.com/java-uuid
    this.uuid = UUID.randomUUID();
    this.name = name;
    this.state = state;
    this.address = address;
    this.country = country;
    this.date_of_birth = dob;
    this.phone_number = phone;
  }

  public String[] getProperties() {
    String[] properties = new String[7];
    properties[0] = this.uuid.toString();
    properties[1] = this.name;
    properties[2] = this.state;
    properties[3] = this.address;
    properties[4] = this.country;
    properties[5] = this.date_of_birth;
    properties[6] = String.valueOf(this.phone_number);
    return properties;
  }

  public static void main(String args[]) {
  }
}