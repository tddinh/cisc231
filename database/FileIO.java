package database;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileIO {
  private static FileSystem fs = FileSystems.getDefault();
  private static String fileName = "database.txt";
  private static Path absPath = convertToAbsolutePath(fileName);

  public static void main(String args[]) {
  }
  public static void read() {
    try {
      InputStream input = Files.newInputStream(absPath);
      BufferedReader reader = new BufferedReader(new InputStreamReader(input));
      String str = reader.readLine();

      while (str != null) {
        if (str instanceof String) {
          str = reader.readLine();
        }
      }
      input.close();
    } catch (Exception e) {
      System.out.println("FileIO.readFile Exception: " + e);
    }
  }

  public static void write(String[] inputs) {
    try {
      // byte[] bytes = convertStringsToBytesArray(inputs);
      // ByteBuffer buffer = ByteBuffer.wrap(bytes);
      OutputStream output = new BufferedOutputStream( Files.newOutputStream(absPath));
      BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
      for (int i = 0; i < inputs.length; i++) {
        String input = inputs[i];
        writer.write(input, 0, input.length());
      }
      writer.close();
    } catch (Exception e) {
      System.out.println("FileIO.readFile Exception: " + e);
    }
  }


  private static Path convertToAbsolutePath(String filename) {
    Path inputPath = Paths.get(filename);
    Path fullPath = inputPath.toAbsolutePath();
    // System.out.println("Full path is " + fullPath.toString());
    return fullPath;
  }

  // private static byte[] convertStringsToBytesArray(String[] data) {
  //   ByteBuffer byteBuffer = ByteBuffer.allocate(data.length * 4);
  //   IntBuffer intBuffer = byteBuffer.asIntBuffer();
  //   intBuffer.put(data);

  //   byte[] array = byteBuffer.array();

  //   for (int i=0; i < array.length; i++) {
  //     System.out.println(i + ": " + array[i]);
  //   }
  // }
}