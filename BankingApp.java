import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import layout.AppMenuBar;

public class BankingApp extends JFrame {
  JFrame frame;
  ActionListener listener = new AppEventSystem(this);

  public BankingApp(String title) {
    super();
    setupWindowFrame(this);
    createAndShowGUI(this);
    setTitle(title);
  }

  private void setupWindowFrame(JFrame frame) {
    final int DEFAULT_WIDTH = 900;
    final int DEFAULT_HEIGHT = 650;
    frame.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    frame.setLayout(new GridLayout(3, 1));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void createAndShowGUI(JFrame frame) {
    // app navigation
    AppMenuBar navmenu = new AppMenuBar(this.listener);
    frame.add(navmenu.component);
    // set initial content
    // ClientLoginForm login = new ClientLoginForm(navmenu);
    // frame.add(login.component);

    // only call after adding all the necessary components, or else it'll show a
    // blank window.
    frame.setVisible(true);
  }

  public static void main(String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        new BankingApp("Mercy Banking");
      }
    });
  }
}
