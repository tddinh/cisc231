
### To Run Project ###

- compile source code `javac BankingApp.java`;

- initialize application `java BankingApp`;

-------------------------------------------------------------------------------------------

  

### Directory Structure ###

We organize our source files based on their purposes, into directories:

-  `api` for our models

-  `layout` for our gui

-  `database`

  

To make the source files available outside of their relative directory path, we must use the package keyword, follow by the name of their relative directory, i.e:

-  `package [directory]`

  

Just like with native java packages, we can import our custom packages into source file by using `import [package_name]`

  

For more information on packages: https://www.geeksforgeeks.org/packages-in-java/

  
  

-----------------------------------------------------------------------------------------

  

### Layout / GUI ###

| Description | Refs |
|----|----|
| Overview of JFrame Components | https://www.cs.cmu.edu/~pattis/15-1XX/15-200/lectures/view/lecture.html
| Information on Layouts | [[Tutorial]: Layout Managers docs](https://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html)
| Information on using Box Layout for our Forms |[[Tutorial]: Box Layout](https://docs.oracle.com/javase/tutorial/uiswing/layout/box.html)
| GUI Components | [Oracle's Swing UI Components docs](https://docs.oracle.com/javase/tutorial/uiswing/components/)
|Action Listener and Events | [Swing Component API docs]( http://www.c-jump.com/bcc/c257c/Week08/Week08.html#W08_0270_event_handling)
|Handling Text Input Events | [Swing Component API docs](https://docs.oracle.com/javase/tutorial/uiswing/components/textfield.html)


- also, chapter 14 of our java programming textbook.