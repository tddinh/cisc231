package layout;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class AppMenuBar {
  public JPanel component;
  public JMenuBar menuBar;

  public AppMenuBar(ActionListener listener) {
    menuBar = new JMenuBar();
    menuBar.add(this.createAccountMenu(listener));
    menuBar.add(this.createTransactionsMenu(listener));
    menuBar.add(Box.createHorizontalGlue());

    component = new JPanel();
    component.setLayout(new FlowLayout());
    component.setSize(200, 200);
    component.add(menuBar);
  }

  private JMenu createAccountMenu(ActionListener listener) {
    JMenu m = new JMenu("Client");
    JMenuItem m1 = new JMenuItem("Login");
    JMenuItem m2 = new JMenuItem("Create New Account");
    m1.addActionListener(listener);
    m2.addActionListener(listener);
    m.add(m1);
    m.add(m2);
    return m;
  }

  private JMenu createTransactionsMenu(ActionListener listener) {
    JMenu m = new JMenu("Transactions");
    JMenuItem m1 = new JMenuItem("View Balance");
    JMenuItem m2 = new JMenuItem("Withdraw");
    JMenuItem m3 = new JMenuItem("Deposit");
    m1.addActionListener(listener);
    m2.addActionListener(listener);
    m3.addActionListener(listener);
    m.add(m1);
    m.add(m2);
    m.add(m3);
    return m;
  }

  public static void main(String args[]) {
  }
}