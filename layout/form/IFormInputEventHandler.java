package layout.form;

interface IFormInputEventHandler {
  String getInputValue(String value);
}