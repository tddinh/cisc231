package layout.form;

import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class FormBuilder {
  public JPanel createPanelRow(String fieldLabel, FormInput field) {
    JPanel panel = new JPanel();
    setDimensions(panel);
    createTextInput(panel, fieldLabel, field);
    return panel;
  }

  /*
   * SupressWarnings is a hack that allows us to avoid compilation error when casting Object to a HashMap
   * https://coderanch.com/t/495393/java/cast-Object-HashMap-unchecked-cast
  **/
  @SuppressWarnings("unchecked")
  public JPanel createPanelRowMultiFields(Map<String, Map<String, FormInput>> fields) {
    JPanel panel = new JPanel();

    for (Map.Entry object : fields.entrySet()) {
      Map<String, FormInput> field =
            (Map<String, FormInput>) object.getValue();
      String label = (String) object.getKey();
      String key = field.keySet().toArray()[0].toString();
      createTextInput(panel, label, field.get(key));
    }

    setDimensions(panel);
    return panel;
  }


  public JButton createSubmitButton(ActionListener listener) {
    JButton button = new JButton();
    button.setText("Submit");
    button.addActionListener(listener);
    return button;
  }

  public void createTextInput(JPanel panel, String fieldLabel, FormInput field) {
    JPanel inner = new JPanel();
    JLabel label = new JLabel(fieldLabel);

    label.setVerticalAlignment(JLabel.TOP);
    label.setHorizontalAlignment(JLabel.LEFT);
    inner.setLayout(new BoxLayout(inner, BoxLayout.Y_AXIS));
    inner.add(label);
    inner.add(field);
    panel.add(inner);
  }

  private static void setDimensions(JPanel panel) {
    panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
    panel.setBorder(new EmptyBorder(0, 80, 0, 80));
  }

  public static void main(String args[]) {
  }
}
