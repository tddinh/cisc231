package layout.form;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.lang.reflect.Method;

/*
 * JTextField requires DocumentListener, instead of ActionListener.
 * Reference: https://docs.oracle.com/javase/8/docs/api/javax/swing/JTextField.html
 * 
 * How to retrieve user input text from Document events
 * Reference: https://docs.oracle.com/javase/8/docs/api/javax/swing/text/Document.html#getText-int-int-
**/
public class FormInput extends JTextField {
  private String id = "";
  private String value = "";

  public FormInput(int cols) {
    super(cols);
    setupEventHandling();
  }

  public String getID() {
    return this.id;
  }

  public void setID(String id) {
    this.id = id;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
    this.printCurrentInputValue();
  }

  // /*
  //  * How to use Interfaces to pass a callack function for events
  //  * Ref: https://stackoverflow.com/questions/4685563/how-to-pass-a-function-as-a-parameter-in-java
  // **/
  // public IFormInputEventHandler<String> getInputValue = (value) -> {
  //   System.out.println("inputEventHandler");
  //   System.out.println(value);
  // }

  private void printCurrentInputValue() {
    System.out.println(this.id + " value: " + this.value);
  }

  private void setupEventHandling() {
    try {
      FormDocumentListener listener = new FormDocumentListener(this);
      getDocument().addDocumentListener(listener);
    } catch (Exception e) {
      System.out.println("FormInput.Error could not attach event handler" + e);
    }
  }


  protected class FormDocumentListener implements DocumentListener {
    private FormInput formInput;

    public FormDocumentListener(FormInput input) {
      this.formInput = input;
    }


    public void insertUpdate(DocumentEvent event) {
      try {
        Document document = event.getDocument();
        String currentValue = formInput.getValue();
        String addedValue = document.getText(
          event.getOffset(),
          event.getLength()
        );
        String value = currentValue + addedValue;
        formInput.setValue(value);
      } catch(Exception e) {
        System.out.println("FormDocumentListener.insertUpdate error: " + e);
      }
    }
    public void removeUpdate(DocumentEvent event) {
      try {
        formInput.setValue(this.formInput.getText());
      } catch(Exception e) {
        System.out.println("FormDocumentListener.insertUpdate error: " + e);
      }
    }
    public void changedUpdate(DocumentEvent e) {
        // displayEditInfo(e);
    }

    private void displayEditInfo(DocumentEvent e) {
            Document document = (Document)e.getDocument();
            int changeLength = e.getLength();
            System.out.println(e.getType().toString() + ": "
                + changeLength + " character"
                + ((changeLength == 1) ? ". " : "s. ")
                + " Text length = " + document.getLength()
                + ".");
    }
  }

  public static void main(String args[]) {
  }
}