package layout;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import layout.form.FormBuilder;
import layout.form.FormInput;

import api.client.Client;
import database.FileIO;


public class ClientSignupForm extends FormBuilder implements IComponent, ActionListener {
  private Map<String, FormInput> fields = new HashMap<String, FormInput>() {{
    put("name", new FormInput(50));
    put("address", new FormInput(50));
    put("state", new FormInput(50));
    put("country", new FormInput(50));
    put("date_of_birth", new FormInput(50));
    put("phone_number", new FormInput(50));
  }};
  private JPanel component;

  public ClientSignupForm() {
    Map<String, Map<String, FormInput>> row1 = getPanelRowParams("name", "address", "Name", "Address");
    Map<String, Map<String, FormInput>> row2 = getPanelRowParams("state", "country", "State", "Country");
    Map<String, Map<String, FormInput>> row3 = getPanelRowParams("date_of_birth", "phone_number", "Date of Birth", "Phone Number");

    JLabel title  = new JLabel("New Client Form");
    JPanel panel1 = createPanelRowMultiFields(row1);
    JPanel panel2 = createPanelRowMultiFields(row2);
    JPanel panel3 = createPanelRowMultiFields(row3);

    JButton submit = createSubmitButton(this);

    component = new JPanel();
    component.add(title);
    component.add(panel1);
    component.add(panel2);
    component.add(panel3);
    component.add(submit);
    component.setLayout(new GridLayout(3, 2, 0, 40));
  }

  public Map<String, Map<String, FormInput>> getPanelRowParams(String field1, String field2, String label1, String label2) {
    Map<String, Map<String, FormInput>> row = new HashMap<String, Map<String, FormInput>>() {{
      put(label1, Map.of(field1, fields.get(field1)));
      put(label2, Map.of(field2, fields.get(field2)));
    }};
    return row;
  }


  @Override
  public void actionPerformed(ActionEvent evt) {
    String name = this.fields.get("name").getText();
    String address = this.fields.get("address").getText();
    String country = this.fields.get("country").getText();
    String state = this.fields.get("state").getText();
    String dob = this.fields.get("date_of_birth").getText();
    int phone = Integer.parseInt(this.fields.get("phone_number").getText());
    Client newClient = new Client(name, address, country, state, dob, phone);
    String[] properties = newClient.getProperties();

    FileIO.write(properties);
  }

  @Override
  public JPanel getComponent() {
    return this.component;
  }

  public static void main(String[] args) {
  }
}
