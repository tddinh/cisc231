package layout;

import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import layout.form.FormBuilder;
import layout.form.FormInput;


public class ClientLoginForm extends FormBuilder implements IComponent, ActionListener {
  private Map<String, FormInput> fields  = new HashMap<String, FormInput>() {{
    put("email", new FormInput(50));
    put("password", new FormInput(50));
  }};
  private JPanel component;


  public ClientLoginForm() {
    JLabel title  = new JLabel("Welcome! Please enter credentials to sign in.");
    JPanel panel1 = createPanelRow("Email", fields.get("email"));
    JPanel panel2 = createPanelRow("Password", fields.get("password"));
    JButton submit = createSubmitButton(this);

    component = new JPanel();
    component.add(title);
    component.add(panel1);
    component.add(panel2);
    component.add(submit);
    component.setLayout(new GridLayout(3, 2, 0, 40));
  }

  @Override
  public void actionPerformed(ActionEvent evt) {
    String email = this.fields.get("email").getText();
    String password = this.fields.get("password").getText();
  }

  @Override
  public JPanel getComponent() {
    return this.component;
  }

  public static void main(String[] args) {
  }
}
