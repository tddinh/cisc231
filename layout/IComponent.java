package layout;
import javax.swing.JPanel;

public interface IComponent {
  public JPanel getComponent();
}