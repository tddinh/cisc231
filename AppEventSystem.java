import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import layout.ClientLoginForm;
import layout.ClientSignupForm;
import layout.IComponent;


public class AppEventSystem implements ActionListener {
  public JFrame frame;
  public IComponent currentComponent;

  public AppEventSystem(JFrame frame) {
    this.frame = frame;
  }

  public static void setCurrentComponent(IComponent component) {
    // this.currentComponent = component;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String selection = e.getActionCommand();
    System.out.println("selection");
    System.out.println(selection);
    switch(selection) {
      case "Login":
        this.updateContent(new ClientLoginForm());
        break;
      case "Create New Account":
        this.updateContent(new ClientSignupForm());
        break;
      case "View Balance":
      case "Deposit":
      case "Withdraw":
      default:
        break;
    }
  }

  public void updateContent(IComponent component) {
    // update content and redraw GUI
    if (currentComponent != null) {
      frame.remove(currentComponent.getComponent());
    }
    frame.add(component.getComponent());
    currentComponent = component;
    this.redrawFrame();
  }

  public void redrawFrame() {
    frame.revalidate();
    frame.repaint();
  }
}
